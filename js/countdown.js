const countDown = () => {
  const delivery = $("#deliveryDate");
  if (delivery) {
    const now = new Date().getTime();
    const deliveryDate = new Date(delivery.val()).getTime();
    if (deliveryDate > now) {
      const gap = deliveryDate - now;
      const second = 1000;
      const minute = second * 60;
      const hour = minute * 60;
      const day = hour * 24;
      const textDay = Math.floor(gap / day);
      const textHour = Math.floor((gap % day) / hour);
      const textMinute = Math.floor((gap % hour) / minute);
      const textSecond = Math.floor((gap % minute) / second);
      if (textDay >= 0) {
        const dayLabel = $("#daysLeft");
        if (dayLabel) {
          dayLabel.html(textDay > 9 ? String(textDay) : "0" + String(textDay));
        }
      }
      if (textHour >= 0) {
        const hourLabel = $("#hoursLeft");
        if (hourLabel) {
          hourLabel.html(
            textHour > 9 ? String(textHour) : "0" + String(textHour)
          );
        }
      }
      if (textMinute >= 0) {
        const minuteLabel = $("#minutesLeft");
        if (minuteLabel) {
          minuteLabel.html(
            textMinute > 9 ? String(textMinute) : "0" + String(textMinute)
          );
        }
      }
      if (textSecond >= 0) {
        const secondLabel = $("#secondsLeft");
        if (secondLabel) {
          secondLabel.html(
            textSecond > 9 ? String(textSecond) : "0" + String(textSecond)
          );
        }
      }
    } else {
      const dayLabel = $("#daysLeft");
      if (dayLabel) {
        dayLabel.html("00");
      }
      const hourLabel = $("#hoursLeft");
      if (hourLabel) {
        hourLabel.html("00");
      }
      const minuteLabel = $("#minutesLeft");
      if (minuteLabel) {
        minuteLabel.html("00");
      }
      const secondLabel = $("#secondsLeft");
      if (secondLabel) {
        secondLabel.html("00");
      }
    }
  }
};
$(document).ready(function () {
  setInterval(() => {
    countDown();
  }, 1000);
});
